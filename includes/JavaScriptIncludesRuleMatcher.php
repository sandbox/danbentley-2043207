<?

/**
 * This class handles the matching of JavaScriptIncludesRules using the
 * supplied url and nodeType properties
 *
 */
class JavaScriptIncludesRuleMatcher {

  /**
   * @var array JavaScriptIncludesRule
   */
  public $rules;

  /**
   * Url to match
   *
   * @var string url
   */
  public $url;

  /**
   * nodeType to match
   *
   * @var string nodeType
   */
  public $nodeType;

  /**
   * Current evaluated rule. Used to avoid having to pass variable to matching
   * methods
   *
   * @var JavaScriptIncludesRule
   */
  public $currentRule;

  /**
   * Delimiter used in doesCurrentRuleMatchURL regex. Default delimiter '/'
   * would require all values to be properly escaped before this function is
   * called.
   *
   * @var string
   */
  public $delimiter = '#';

  /**
   * Sets internal rules property
   *
   * @param array JavaScriptIncludesRule
   */
  public function setRules($rules)
  {
    $this->rules = $rules;
  }

  /**
   * Sets internal url property
   *
   * @param string url
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  /**
   * Sets internal nodetype property
   *
   * @param string nodeType
   */
  public function setNodeType($nodeType)
  {
    $this->nodeType = $nodeType;
  }

  /**
   * Checks whether rule's regex ($this->currentRule->urlPatter) matches the supplied
   * URL ($this->url).
   *
   * @param bool success
   */
  public function doesCurrentRuleMatchURL()
  {
    $urlPattern = $this->currentRule->urlPattern;
    if (!$urlPattern) {
      return false;
    }

    return (preg_match($this->delimiter . '^' . $urlPattern . '$' .  $this->delimiter, $this->url) === 1);
  }

  /**
   * Checks whether rule's nodeType ($this->currentRule->nodeType) matches the supplied
   * nodeType ($this->nodeType).
   *
   * @param bool success
   */
  public function doesCurrentRuleMatchNodeType()
  {
    $ruleNodeType = $this->currentRule->nodeType;
    if (!$ruleNodeType || !$this->nodeType) {
      return false;
    }

    return ($ruleNodeType === $this->nodeType);
  }

  /**
   * Calls matchMultiple() and retunrs the first matched rule
   *
   * @see matchMultiple
   *
   * @return JavaScriptIncludesRule matchedRule
   */
  public function match()
  {
    $matchedRules = $this->matchMultiple();
    return (count($matchedRules) > 0) ? $matchedRules[0] : null;
  }

  /**
   * Iterates through rules to find a match either by URL or node type.
   *
   * @see doesCurrentRuleMatchNodeType
   * @see doesCurrentRuleMatchURL
   *
   * @return array JavaScriptIncludesRule matchedRules
   */
  public function matchMultiple()
  {
    $matchedRules = array();

    foreach($this->rules as $rule) {
      $this->currentRule = $rule;
      if ($this->doesCurrentRuleMatchURL() || $this->doesCurrentRuleMatchNodeType()) {
        $matchedRules[] = $rule;
      }
    }

    return $matchedRules;
  }
}
