<?

/**
 * Implements hook_form
 *
 * Form used to create or edit JavaScriptIncludesRule. 
 *
 * Existing JavaScriptIncludesRule are passed using drupal_get_form
 *
 * @see drupal_get_form
 */
function require_js_manager_add_form($form_id, $form_info) {

  $args = $form_info['build_info']['args'];
  $rule = (isset($args[0])) ? $args[0] : new JavaScriptIncludesRule();

  return array('add_form' => array(
    'id' => array(
      '#type' => 'hidden',
      '#default_value' => $rule->id . "",
    ),
    'urlPattern' => array(
      '#type' => 'textfield',
      '#title' => t('URL pattern'),
      '#default_value' => $rule->urlPattern,
      '#description' => t('The Regex used to match again the request_path() of the current page'),
    ),
    'nodeType' => array(
      '#type' => 'select',
      '#title' => t('Content types'),
      '#options' => require_js_manager_get_node_types(),
      '#default_value' => $rule->nodeType,
    ),
    'file' => array(
      '#type' => 'textfield',
      '#title' => t('JavaScript file'),
      '#default_value' => $rule->file,
      '#description' => t('The JavaScript file to include should the rules match. Path is relative to the base of the current theme.'),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  ));
}

/**
 * Fetches a list of node types for use in the selects
 *
 * @see node_type_get_types
 */
function require_js_manager_get_node_types() {
  $nodeTypes = array_map(function($type) {
    return $type->name;
  }, node_type_get_types());

  return array_merge(array('none' => 'None'), $nodeTypes);
}

/**
 * Implements hook_form_submit
 *
 * Create/update JavaScriptIncludesRule and redirect back to index page
 */
function require_js_manager_add_form_submit($form, &$form_state) {
  // TODO Ensure that we have values to work with
  $values = $form_state['values'];

  $rule = new JavaScriptIncludesRule(array(
    'id' => $values['id'],
    'urlPattern' => $values['urlPattern'],
    'nodeType' => $values['nodeType'],
    'file' => $values['file'],
  ));

  $ruleManager = new JavaScriptIncludesRuleManager();
  $ruleManager->add($rule);
  $ruleManager->save();

  drupal_set_message(t('The form has been submitted.'));
  drupal_goto(REQUIRE_JS_MANAGER_URL);
}

/**
 * Fetch a rule using the given id and pass to require_js_manager_add_form via 
 * drupal_get_form
 *
 * @see drupal_get_form
 */
function require_js_manager_edit($id) {
  $ruleManager = new JavaScriptIncludesRuleManager();
  $rule = $ruleManager->getById($id);

  return render(drupal_get_form('require_js_manager_add_form', $rule));
}
