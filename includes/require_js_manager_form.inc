<?

/**
 * Implements hook_form
 *
 * Create form items from all stored JavaScriptIncludesRules.
 *
 * Each item also has a weight so we can prioritise rules.
 *
 */
function require_js_manager_form($form_id, $form_info) {

  // Identify that the elements in 'example_items' are a collection, to
  // prevent Form API from flattening the array when submitted.
  $form['row_items']['#tree'] = TRUE;

  $ruleManager = new JavaScriptIncludesRuleManager();
  $rules = $ruleManager->fetch();
  foreach($rules as $rule) {

    $editURL = str_replace('%', $rule->id, REQUIRE_JS_MANAGER_EDIT_URL);
    $deleteURL = str_replace('%', $rule->id, REQUIRE_JS_MANAGER_DELETE_URL);

    $form['row_items'][$rule->id] = array(
      'id' => array(
        '#markup' => $rule->id,
      ),
      'urlPattern' => array(
        '#markup' => $rule->urlPattern,
      ),
      'nodeType' => array(
        '#markup' => $rule->nodeType,
      ),
      'file' => array(
        '#markup' => $rule->file,
      ),
      'actions' => array(
        '#markup' => '<a href="/' . $editURL . '">Edit</a> <a href="/' . $deleteURL . '">Delete</a>',
      ),

      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $rule->weight,
        '#delta' => 10,
        '#title-display' => 'invisible',
      ),
    );
  }

  $form['includeMinifiedFiles'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get(REQUIRE_JS_MANAGER_SHOULD_MINIFY_KEY),
    '#title' => t('Use minified files'),
    '#description' => t('<p>Non-minified source should be included in the rules to allow us to use this checkbox to switch minified source on/off during development</p><p>For require.js files, run the optimmiser <code><b>r.js -o build.js</b></code></p><p>Minified source should live in <code><b>&lt;path-to-current-theme&gt;/js/build</b></code></p>'),
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Changes')
  );
  return $form;
}

/**
 * Returns a styled form.
 *
 * The require_js_manager_form gets wrapped in a sortable table
 */
function theme_require_js_manager_form($vars) {

  $form = $vars['form'];

  $header = array(
    t('id'),
    t('URL pattern'),
    t('node type'),
    t('File'),
    t('Weight'),
    t('Actions'),
  );

  $rows = array();

  foreach(element_children($form['row_items']) as $id) {

    // Assign by reference (sigh) so Drupal can mark the field as rendered.
    $item =& $form['row_items'][$id];

    $form['row_items'][$id]['weight']['#attributes']['class'] = array('item-weight');
    
    $rows[$id] = array(
      'data' => array(
        drupal_render($item['id']),
        drupal_render($item['urlPattern']),
        drupal_render($item['nodeType']),
        drupal_render($item['file']),
        drupal_render($item['weight']),
        drupal_render($item['actions']),
      ),
      'class' => array('draggable'),
    );
  }

  $output = '<a href="/' . REQUIRE_JS_MANAGER_ADD_URL . '">Add rule</a>';
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'javascript-includes-table'
    )
  ));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('javascript-includes-table', 'order', 'sibling', 'item-weight');

  return $output;
}

/**
 * Implements hook_form_submit
 *
 * Iterates through stored rules and saves weights.
 */
function require_js_manager_form_submit($form, &$form_state) {

  $ruleManager = new JavaScriptIncludesRuleManager();

  $values = $form_state['values'];

  variable_set(REQUIRE_JS_MANAGER_SHOULD_MINIFY_KEY, $values['includeMinifiedFiles']);
  foreach($values['row_items'] as $id => $item) {
    $rule = $ruleManager->getById($id);
    $rule->weight = $item['weight'];
  }

  $ruleManager->save();
}

