<?

class JavaScriptIncludesRule {

  /**
   * @var mixed id
   */
  public $id;

  /**
   * @var string urlPattern
   */
  public $urlPattern;

  /**
   * @var string nodeType
   */
  public $nodeType;

  /**
   * @var string file
   *
   * File to load if this rule matches
   */
  public $file;

  /**
   * @var int weight
   *
   * Priority given to this rule. Only the first matched rule will be executed.
   */
  public $weight;

  /**
   * Constructor
   *
   * @param array. All properties can be set using the correct key/values pairs
   * in the config array
   */
  function __construct($config=array())
  {
    $this->id = (isset($config['id'])) ? $config['id'] : null;
    $this->urlPattern = (isset($config['urlPattern'])) ? $config['urlPattern'] : null;
    $this->nodeType = (isset($config['nodeType'])) ? $config['nodeType'] : null;
    $this->file = (isset($config['file'])) ? $config['file'] : null;
    $this->weight = (isset($config['weight'])) ? $config['weight'] : null;
  }
}
