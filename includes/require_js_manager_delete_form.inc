<?

/**
 * Fetches a rule from a given id and removes from manager.
 *
 * Redirects to index page on success
 */
function require_js_manager_delete($id) {
  $ruleManager = new JavaScriptIncludesRuleManager();
  $rule = $ruleManager->getById($id);

  $ruleManager->remove($rule);
  $ruleManager->save();

  drupal_goto(REQUIRE_JS_MANAGER_URL);
}
