<?

require_once 'JavaScriptIncludesRule.php';

/**
 * This class handles the storage and retrieval of JavaScriptIncludesRules
 * using drupals variable_(get|set) functions.
 *
 * @see variable_get
 * @see variable_set
 */

class JavaScriptIncludesRuleManager {

  const REQUIRE_JS_MANAGER_RULES_KEY = 'javascript_includes_rules';

  /**
   * @var array rules
   */
  public $rules;

  /**
   * Note that creating an instance has the side effect of fetching the rules
   *
   */
  public function __construct()
  {
    $this->rules = $this->fetch();
  }

  /**
   * Add a JavaScriptIncludesRule to internal array. Note this rule isn't
   * persisted until save() is called.
   *
   * If the rule matches (by id) and existing rule, we update the record
   *
   * @param JavaScriptIncludesRule $ruleToAdd
   */
  public function add(JavaScriptIncludesRule $ruleToAdd)
  {
    if (!$ruleToAdd->id) {
      $ruleToAdd->id = count($this->rules) + 1;
    }
    $this->rules[$ruleToAdd->id] = $ruleToAdd;
  }

  /**
   * Removes a JavaScriptIncludesRule to internal array. Note this rule isn't
   * persisted until save() is called.
   *
   * @param JavaScriptIncludesRule $ruleToRemove
   */
  public function remove(JavaScriptIncludesRule $ruleToRemove)
  {
    foreach($this->rules as $index => $rule) {
      if ($rule->id == $ruleToRemove->id) {
        unset($this->rules[$index]);
        break;
      }
    }
  }

  /**
   * Fetch a JavaScriptIncludesRule that matches the given id.
   *
   * @returns JavaScriptIncludesRule
   */
  public function getById($id)
  {
    foreach($this->rules as $rule) {
      if ($rule->id == $id) {
        return $rule;
      }
    }
  }

  /**
   * Persist contents of $this->rules into Drupal variable store.
   *
   * This method shouldn't be called internally.
   *
   * @returns bool success
   */
  public function save()
  {
    return variable_set(self::REQUIRE_JS_MANAGER_RULES_KEY, $this->rules);
  }

  /**
   * Fetch all stored JavaScriptIncludesRules
   *
   * @returns array JavaScriptIncludesRules
   */
  public function fetch()
  {
    $rules =  variable_get(self::REQUIRE_JS_MANAGER_RULES_KEY, array());

    // Sort keys by weight. Highest first.
    usort($rules, function($a, $b) {
      return ($a->weight > $b->weight) ? - 1 : 1;
    });

    // Reapply the keys before returning
    $keys = array_map(function($rule) {
      return $rule->id;
    }, $rules);

    return array_combine($keys, $rules);
  }

  /**
   * Removes all records from Drupal internal store.
   *
   * Note this method doesn't clear the internal rules array.
   *
   * Should only be used for debug purposes
   *
   * @returns bool success
   */
  public function clear()
  {
    return variable_set(self::REQUIRE_JS_MANAGER_RULES_KEY, array());
  }
}
